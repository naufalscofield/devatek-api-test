<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Mail\DevatekEmail;
use App\User;

class AuthController extends Controller
{
     /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function postLogin(Request $request)
    {
        $email  =   $request->input('email');
        $password  =   $request->input('password');

        $user = User::where('email', $email)->first();

        $this->validate($request, [
            'email'    => 'required|email|max:255',
            'password' => 'required',
        ]);

        try {

            if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
                // return response()->json(['user_not_found'], 404);
                return response()->json([
                    'status' => false,
                    'message' => 'User Not Found!',
                    'data' => ''
                ], 200);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent' => $e->getMessage()], 500);

        }

        // return response()->json(compact('token'));
        return response()->json(
            [
            'status' => true,
            'message' => 'login success',
            'data' => [
                'user' => $user,
                "token" => $token
            ],
            // "token" => (compact('token'))
            ], 200);
    }

    public function register(Request $request)
    {
        $no_telp = $request->input('no_telp');
        $email = $request->input('email');
        $password = Hash::make($request->input('password'));
        
        $register = User::create([
            'no_telp' => $no_telp,
            'email' => $email,
            'password' => $password,
            ]);
            
            

        if($register){
            Mail::to($email)->send(new DevatekEmail());
            return response()->json ([
                'status' => true,
                'message' => 'Register Success!',
                'data' => $register
            ], 201);
            
        } else {
            return response()->json ([
                'status' => false,
                'message' => 'Register Fail!',
                'data' => ''
            ], 400);
        }
    }

    public function login(Request $request)
    {
        $email  =   $request->input('email');
        $password  =   $request->input('password');

        $user = User::where('email', $email)->first();

        if ($user){
            if (Hash::check($password, $user->password)) {
                $apiToken = base64_encode(str_random(40));
                $user->update([
                    'api_token' => $apiToken
                ]);
    
                return response()->json([
                    'status' => true,
                    'message' => 'login success',
                    'data' => [
                        'user' => $user,
                        'api_token' => $apiToken
                    ]
                    ], 201);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'login fail wrong password',
                    'data' => [
                        'email' => $email,
                        'password' => $password
                    ]
                ], 200);
    
            }

        } else {
            return response()->json([
                'status' => false,
                'message' => 'email not registered',
                'data' => [
                    'email' => $email,
                    'password' => $password
                ]
            ], 200);
        }
    }

}
